#pragma once
#include <iostream>
#include "FileData.h"
#include "Matrix\DiagonaMatrix.h"
#include <math.h>
#include <list>
#include "Point.h"
#include <string>
#include <iomanip>

using namespace std;

class SplajnsInterpolation
{
public:
	SplajnsInterpolation();
	SplajnsInterpolation(FileData* inputData);
	void doSplain();
	list<Point> getResultsPoints();
	void showResultY();
	void saveResultsToFile();
	~SplajnsInterpolation();
private:
	FileData* data;
	list<Point> resultPoints;
	void LUPivotingDecompose(MatrixNxM* a, MatrixNxM* pivots);
	void LUPivotingSolve(MatrixNxM a, MatrixNxM pivots, MatrixNxM b, MatrixNxM& x);
	double calculateDeltaX();
	MatrixNxM calculateSplainMatrix();
};

