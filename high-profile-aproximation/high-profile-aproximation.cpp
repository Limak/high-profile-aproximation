// high-profile-aproximation.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <fstream>
#include "FileData.h"
#include "LagrangeInterpolation.h"
#include "SplajnsInterpolation.h"

using namespace std;
#define _CRT_SECURE_NO_WARNINGS 1
#define TEST_1 5
#define TEST_2 10
#define TEST_3 15
#define TEST_4 20

int main(int argc, char** argv)
{
	char* filename = "";

	if (argc == 2)
	{
		swap(filename, argv[1]);
	}
	else
	{
		cout << "nie wprowadzono nazwy pliku" << endl;
		filename = "";
		return 0;
	}

	FileData* inputTest1 = new FileData(filename, TEST_1);
	FileData* inputTest2 = new FileData(filename, TEST_2);
	FileData* inputTest3 = new FileData(filename, TEST_3);
	FileData* inputTest4 = new FileData(filename, TEST_4);

	inputTest1->readData();
	inputTest2->readData();
	inputTest3->readData();

	//TEST 1
	cout << "========== TEST 1 ==========" << endl;
	LagrangeInterpolation* test1 = new LagrangeInterpolation(inputTest1);
	SplajnsInterpolation* splainTest1 = new SplajnsInterpolation(inputTest1);
	cout << "Lagrange" << endl;
	test1->doLagrange();
	test1->showResultPoints();
	test1->saveResultsToFile();
	cout << endl;
	cout << "splajny" << endl;
	splainTest1->doSplain();
	splainTest1->showResultY();
	cout << endl;
	splainTest1->saveResultsToFile();

	//TEST 2
	cout << "========== TEST 2 ==========" << endl;
	LagrangeInterpolation* test2 = new LagrangeInterpolation(inputTest2);
	SplajnsInterpolation* splainTest2 = new SplajnsInterpolation(inputTest2);
	test2->doLagrange();
	cout << "Lagrange" << endl;
	test2->showResultPoints();
	test2->saveResultsToFile();
	cout << endl;
	cout << "splajny" << endl;
	splainTest2->doSplain();
	splainTest2->showResultY();
	cout << endl;
	splainTest2->saveResultsToFile();

	//TEST 3
	cout << "========== TEST 3 ==========" << endl;
	LagrangeInterpolation* test3 = new LagrangeInterpolation(inputTest3);
	SplajnsInterpolation* splainTest3 = new SplajnsInterpolation(inputTest3);
	test3->doLagrange();
	test3->showResultPoints();
	test3->saveResultsToFile();
	cout << endl;
	cout << "splajny" << endl;
	splainTest3->doSplain();
	splainTest3->showResultY();
	cout << endl;
	splainTest3->saveResultsToFile();


	delete inputTest1;
	delete inputTest2;
	delete inputTest3;
	delete inputTest4;
	return 0;
}

