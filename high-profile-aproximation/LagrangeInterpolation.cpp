#include "LagrangeInterpolation.h"



LagrangeInterpolation::LagrangeInterpolation()
{
}

LagrangeInterpolation::LagrangeInterpolation(FileData * inputData)
{
	data = inputData;
}

double LagrangeInterpolation::F(double x)
{
	double result = 0.0;
	calculateComponents(x);
	int heightIndex = 0;

	for (auto& i : components)
	{
		result += i*data->getHeights()[heightIndex];
		heightIndex++;
	}
	return result;
}

void LagrangeInterpolation::showData()
{
	for (int i = 0; i < data->getPointsAmount(); i++)
	{
		double tmpDistance = data->getDistances()[i];
		double tmpHeight = data->getHeights()[i];
		cout << tmpDistance << "\t" << tmpHeight << endl;
	}
}

void LagrangeInterpolation::showComponents()
{
	for (auto& i : components) {
		cout << i << "\t";
	}
	cout << endl;
}

void LagrangeInterpolation::showResultPoints()
{
	for (auto& i : resultPoints)
	{
		cout <<setprecision(9)<< i.y << "\t";
	}
}

void LagrangeInterpolation::saveResultsToFile()
{
	ofstream out;
	string filename = "wynik_lagrange_";
	string pointsAmountStr = to_string(data->getPointsAmount());
	filename.append(data->getFilename()).append("pkt").append(pointsAmountStr).append(".txt");
	out.open(filename);

	for (auto& i : resultPoints)
	{
		out << setprecision(15) << i.x << "\t" << setprecision(15) << i.y << "\n";
	}
	out.close();
}

void LagrangeInterpolation::doLagrange()
{
	double deltaX = data->getDistances()[1] - data->getDistances()[0];
	double lastDistance = data->getDistances()[data->getPointsAmount() - 1];
	Point point;

	for (double i = 0; i < lastDistance; i+=deltaX)
	{
		point.x = i;
		point.y = F(i);
		resultPoints.push_back(point);
	}
}

LagrangeInterpolation::~LagrangeInterpolation()
{
}

void LagrangeInterpolation::calculateComponents(double x)
{
	components.clear();
	double component;
	for (int i = 0; i < data->getPointsAmount(); i++)
	{
		double temporaryNominator = 1.0;
		double temporaryDenominator = 1.0;
		for (int j = 0; j < data->getPointsAmount(); j++)
		{
			double xi = data->getDistances()[i];
			double xj = data->getDistances()[j];
			if (i != j)
			{
				temporaryNominator *= x - xj;
				temporaryDenominator *= xi - xj;

				component = temporaryNominator / temporaryDenominator;
			}
			else
				continue;
		}
		components.push_front(component);
	}
}
