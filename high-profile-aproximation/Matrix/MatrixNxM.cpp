#include "MatrixNxM.h"

MatrixNxM::MatrixNxM()
{
}

MatrixNxM::MatrixNxM(int n, int m)
{
	rows = n;
	columns = m;
	matrix = new float*[rows];

	for (int i = 0; i < rows; i++)
	{
		matrix[i] = new float[columns];
	}

	clearMatrix();
}

int MatrixNxM::getRowsAmount()
{
	return rows;
}

int MatrixNxM::getColumnsAmount()
{
	return columns;
}

void MatrixNxM::setValue(int row, int column, float value)
{
	matrix[row][column] = value;
}

float ** MatrixNxM::getMatrix()
{
	return matrix;
}


MatrixNxM::~MatrixNxM()
{
	/*
	for (int i = 0; i < rows; i++)
	{
		delete[] matrix[i];
	}
	delete[] matrix;
	*/
}

void MatrixNxM::clearMatrix()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matrix[i][j] = 0;
		}
	}
}

void MatrixNxM::copyMatrix(float ** srcMatrix)
{
}
