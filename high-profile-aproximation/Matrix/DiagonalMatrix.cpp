#include "DiagonaMatrix.h"



DiagonalMatrix::DiagonalMatrix()
{
}

DiagonalMatrix::DiagonalMatrix(int n) : MatrixNxM(n, n)
{
}

DiagonalMatrix::DiagonalMatrix(int n, float numbers[3]) : MatrixNxM(n, n)
{
	a1 = numbers[0];	//a1
	a2 = numbers[1];	//a2
	a3 = numbers[2];	//a3
	/*
	lowTriangle = new float*[n];
	upperTriangle = new float*[n];
	diagonalTriangle = new float*[n];
	
	for (int i = 0; i < n; i++)
	{
		lowTriangle[i] = new float[n];
		upperTriangle[i] = new float[n];
		diagonalTriangle[i] = new float[n];
	}
	createLDU();
	*/
}

void DiagonalMatrix::fillFromSelected(int beginningRow, int beginningColumn, float value)
{
	int j = beginningColumn;
	for (int i = beginningRow; i < rows; i++)
	{
		if (j < columns && i < rows)
		{
			matrix[i][j] = value;
			++j;
		}
		else
			break;
	}
}

void DiagonalMatrix::fillDiagonal()
{
	fillFromSelected(0, 0, a1);
	fillFromSelected(0, 1, a2);
	fillFromSelected(0, 2, a3);
	fillFromSelected(1, 0, a2);
	fillFromSelected(2, 0, a3);
}

void DiagonalMatrix::showMatrix()
{
	ofstream out;
	out.open("macierz.txt");
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			out << matrix[i][j] << " ";
		}
		out << "\n";
	}
	out.close();
}

float ** DiagonalMatrix::getMatrix()
{
	return matrix;
}

void DiagonalMatrix::setValue(int row, int column, float value)
{
	matrix[row][column] = value;
}

float ** DiagonalMatrix::getL()
{
	return lowTriangle;
}

float ** DiagonalMatrix::getU()
{
	return upperTriangle;
}

float ** DiagonalMatrix::getD()
{
	return diagonalTriangle;
}


DiagonalMatrix::~DiagonalMatrix()
{
}

void DiagonalMatrix::createLDU()
{
	int diagonalSize = columns;	//bez znaczenia bo macierz kwadratowa
	for (int i = 0; i < diagonalSize; i++)
	{
		for (int j = 0; j < diagonalSize; j++)
		{
			if (i < j)
				upperTriangle[i][j] = matrix[i][j];
			else if (i > j)
				lowTriangle[i][j] = matrix[i][j];
			else
				diagonalTriangle[i][j] = matrix[i][j];
		}
	}
}
