#pragma once
#include "FileData.h"
#include <list>
#include "Point.h"
#include <iomanip>
#include <string>

using namespace std;

class LagrangeInterpolation
{
public:
	LagrangeInterpolation();
	LagrangeInterpolation(FileData* inputData);
	double F(double x);
	void showData();
	void showComponents();
	void showResultPoints();
	void saveResultsToFile();
	void doLagrange();
	void getResultPoints();
	~LagrangeInterpolation();
private:
	FileData* data;
	list<double> components;
	list<Point> resultPoints;

	void calculateComponents(double x);
};

