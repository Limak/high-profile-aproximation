#pragma once
#include <iostream>
#include <fstream>

#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

class FileData
{
public:
	FileData();
	FileData(char* filename, int testPointsAmount);
	void readData();
	char* getFilename();
	double* getDistances();
	double* getHeights();
	int getPointsAmount();
	~FileData();
private:
	char* filename;
	int pointsAmount;
	double* distances;
	double* heights;
};

