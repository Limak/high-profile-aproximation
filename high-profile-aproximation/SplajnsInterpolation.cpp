#include "SplajnsInterpolation.h"

SplajnsInterpolation::SplajnsInterpolation()
{
}

SplajnsInterpolation::SplajnsInterpolation(FileData * inputData)
{
	data = inputData;
}

void SplajnsInterpolation::doSplain()
{
	double deltaX = calculateDeltaX();
	Point tmpPoint;
	MatrixNxM coefficientMatrix = calculateSplainMatrix();

	int lastIndex = data->getPointsAmount()-1;
	double lastValue = data->getDistances()[lastIndex];
	
	for (double i = 0.0; i < lastValue; i += deltaX)
	{
		tmpPoint.x = i;
		double result = 0.0;

		//zalozenie: rowne odstepy

		int xRange = i*(data->getPointsAmount() - 1) / lastValue;

		for (int j = 0; j < 4; j++)
		{
			double x = i - data->getDistances()[xRange];
			//result += coefficientMatrix[4 * xRange + j][0] * pow(x, j);
			double tmpPow = pow(x, j);
			double tmpMatrixElement = coefficientMatrix.getMatrix()[4 * xRange + j][0];
			double tmpValue = coefficientMatrix.getMatrix()[4 * xRange + j][0] * pow(x, j);
			result += tmpValue;
		}
		tmpPoint.y = result;
		resultPoints.push_back(tmpPoint);
	}
}

MatrixNxM SplajnsInterpolation::calculateSplainMatrix()
{
	int coefficientsNumber = 4 * data->getPointsAmount() - 1;
	MatrixNxM coefficients(coefficientsNumber, coefficientsNumber);
	MatrixNxM resultVector(coefficientsNumber, 1);

	double h = data->getDistances()[1] - data->getDistances()[0];

	int offset = 0;
	int tmpFirstDerivative = 1;
	int tmpSecondDerivative = 2;

	for (int i = 0; i < coefficientsNumber; i++)
	{

		//wypelnienie resultsVector
		if (i == 0)
		{
			resultVector.setValue(i, 0, data->getHeights()[i]);
		}
		else if (i == (coefficientsNumber / 2) - 1)
		{
			int lastIndex = data->getPointsAmount() - 1;
			resultVector.setValue(i, 0, data->getHeights()[lastIndex]);
		}
		else if (i < coefficientsNumber / 2 && i % 2 != 0)
		{
			resultVector.setValue(i, 0, data->getHeights()[(i / 2) + 1]);
			resultVector.setValue(i + 1, 0, data->getHeights()[(i / 2) + 1]);
		}
		else if (i >= coefficientsNumber / 2)
		{
			resultVector.setValue(i, 0, 0);
		}


		for (int j = 0; j < coefficientsNumber; j++)
		{
			//wypelnienienie macierzy wspolczynnikow
			if ((i % 2 == 0) && i < coefficientsNumber / 2)
			{
				if (j == i * 2)
				{
					coefficients.setValue(i, j, 1);

				}
				else
				{
					coefficients.setValue(i, j, 0);
				}
			}
			else if (i < coefficientsNumber / 2)
			{
				if (j >= offset && j < offset + 4)
				{
					if (j % 4 == 0)
						coefficients.setValue(i, j, pow(h, 0));
					else if (j % 4 == 1)
						coefficients.setValue(i, j, pow(h, 1));
					else if (j % 4 == 2)
						coefficients.setValue(i, j, pow(h, 2));
					else
						coefficients.setValue(i, j, pow(h, 3));
				}
				else
					coefficients.setValue(i, j, 0);
			}
			else if (i == coefficientsNumber - 2)
			{
				if (j == 2)
					coefficients.setValue(i, j, 2);
				else
					coefficients.setValue(i, j, 0);
			}
			else if (i == coefficientsNumber - 1)
			{
				if (j == coefficientsNumber - 2)
					coefficients.setValue(i, j, 2);
				else if (j == coefficientsNumber - 1)
					coefficients.setValue(i, j, 6 * h);
				else
					coefficients.setValue(i, j, 0);
			}
			else
			{
				//pierwsza pochodna
				if (i % 2 == 0)
				{
					if (j == tmpFirstDerivative)
						coefficients.setValue(i, j, 1);
					else if (j == tmpFirstDerivative + 1)
						coefficients.setValue(i, j, 2 * h);
					else if (j == tmpFirstDerivative + 2)
						coefficients.setValue(i, j, 3 * h*h);
					else if (j == tmpFirstDerivative + 4)
						coefficients.setValue(i, j, -1);
					else
						coefficients.setValue(i, j, 0);
				}
				//druga pochodna
				else
				{
					if (j == tmpSecondDerivative)
						coefficients.setValue(i, j, 2);
					else if (j == tmpSecondDerivative + 1)
						coefficients.setValue(i, j, 6 * h);
					else if (j == tmpSecondDerivative + 4)
						coefficients.setValue(i, j, -2);
					else
						coefficients.setValue(i, j, 0);
				}
			}
		}

		if (i % 2 == 1)
		{
			offset += 4;
			if (i >= coefficientsNumber / 2)
				tmpSecondDerivative += 4;
		}
		else
		{
			if (i >= coefficientsNumber / 2)
				tmpFirstDerivative += 4;
		}

	}


	MatrixNxM pivots(coefficientsNumber + 1, 1);
	LUPivotingDecompose(&coefficients, &pivots);
	MatrixNxM x(coefficientsNumber, coefficientsNumber);
	LUPivotingSolve(coefficients, pivots, resultVector, x);

	return x;
}

list<Point> SplajnsInterpolation::getResultsPoints()
{
	return resultPoints;
}

void SplajnsInterpolation::showResultY()
{
	for (auto& i : resultPoints)
	{
		cout <<setprecision(10) << i.y << "\t";
	}
}

void SplajnsInterpolation::saveResultsToFile()
{
	ofstream out;
	string filename = "wynik_splainy_";
	string pointsAmountStr = to_string(data->getPointsAmount());
	filename.append(data->getFilename()).append("pkt").append(pointsAmountStr).append(".txt");
	out.open(filename);

	for (auto& i : resultPoints)
	{
		out << setprecision(15) << i.x << "\t" << setprecision(15) << i.y << "\n";
	}
	out.close();
}

SplajnsInterpolation::~SplajnsInterpolation()
{
}

void SplajnsInterpolation::LUPivotingDecompose(MatrixNxM * a, MatrixNxM * pivots)
{
	int i;
	int j;
	int k;
	int rowIndexMax;
	double maxValueFromAllRows;
	double *temporaryRowValues = new double[a->getRowsAmount()];
	double absMaxValue;

	for (i = 0; i <= a->getRowsAmount(); i++)
	{
		pivots->setValue(i, 0, i);
	}

	for (i = 0; i < a->getRowsAmount(); i++)
	{
		maxValueFromAllRows = 0.0;
		rowIndexMax = i;

		for (k = i; k < a->getRowsAmount(); k++)
		{
			double tmpKJ = a->getMatrix()[k][i];
			absMaxValue = abs(tmpKJ);

			if (absMaxValue > maxValueFromAllRows)
			{
				maxValueFromAllRows = absMaxValue;
				rowIndexMax = k;
			}
		}

		//zamiana wierszami
		if (rowIndexMax != i)
		{
			j = pivots->getMatrix()[i][0];
			double tmpValue = pivots->getMatrix()[rowIndexMax][0];
			pivots->setValue(i, 0, tmpValue);
			pivots->setValue(rowIndexMax, 0, j);

			for (int z = 0; z < a->getRowsAmount(); z++)
			{
				temporaryRowValues[z] = a->getMatrix()[i][z];
			}

			for (int z = 0; z < a->getRowsAmount(); z++)
			{
				double tmpValue2 = a->getMatrix()[rowIndexMax][z];
				a->setValue(i, z, tmpValue2);
				tmpValue2 = temporaryRowValues[z];
				a->setValue(rowIndexMax, z, tmpValue2);
			}

			double tmpValue2 = 0.0;
			int tmpIndex = a->getRowsAmount()-1;
			tmpValue2 = a->getMatrix()[tmpIndex][0];
			tmpValue2++;

			pivots->setValue(a->getRowsAmount(), 0, tmpValue2);
		}

		for (j = i + 1; j < a->getRowsAmount(); j++)
		{
			double tmpAjiDivAii = a->getMatrix()[j][i];
			tmpAjiDivAii /= a->getMatrix()[i][i];
			a->setValue(j, i, tmpAjiDivAii);

			for (k = i + 1; k < a->getRowsAmount(); k++)
			{
				double tmpAjiMulAik = a->getMatrix()[j][k];
				tmpAjiMulAik -= a->getMatrix()[j][i] * a->getMatrix()[i][k];
				a->setValue(j, k, tmpAjiMulAik);
			}
		}
	}



}

void SplajnsInterpolation::LUPivotingSolve(MatrixNxM a, MatrixNxM pivots, MatrixNxM b, MatrixNxM & x)
{
	for (int i = 0; i < a.getRowsAmount(); i++)
	{
		int tmpIndex = pivots.getMatrix()[i][0];
		double tmpB = b.getMatrix()[tmpIndex][0];
		x.setValue(i, 0, tmpB);

		for (int k = 0; k < i; k++)
		{ 
			double tmpAik = a.getMatrix()[i][k];
			double tmpXk0 = x.getMatrix()[k][0];
			double tmpXi0 = x.getMatrix()[i][0];
			tmpXi0 -= tmpAik*tmpXk0;
			x.setValue(i, 0, tmpXi0);
		}
	}
	/*
	for (int i = 0; i < a.getRowsAmount(); i++)
	{
		cout << a.getMatrix()[i][i] << "\t";
	}
	*/


	for (int i = a.getRowsAmount() - 1; i >= 0; i--)
	{
		for (int k = i + 1; k < a.getRowsAmount(); k++)
		{
			
			double tmpAik = a.getMatrix()[i][k];
			double tmpXk0 = x.getMatrix()[k][0];
			double tmpXi0 = x.getMatrix()[i][0];
			tmpXi0 -= tmpAik*tmpXk0;
			x.setValue(i, 0, x.getMatrix()[i][0] - (a.getMatrix()[i][k] * x.getMatrix()[k][0]));
			//x.setValue(i, 0, tmpXi0);
			
			//tmpXi0 = x.getMatrix()[i][0];
			//tmpXi0 -= a.getMatrix()[i][k] * x.getMatrix()[k][0];
			//double testXi0 = tmpXi0;
			
		}
		//tmpXi0 = x.getMatrix()[i][0] / a.getMatrix()[i][i];
		double test = x.getMatrix()[i][0];
		double testDiagonal = a.getMatrix()[i][i];
		double test2 = x.getMatrix()[i][0] / a.getMatrix()[i][i];
		x.setValue(i, 0, x.getMatrix()[i][0] / a.getMatrix()[i][i]);
	}
}

double SplajnsInterpolation::calculateDeltaX()
{
	return data->getDistances()[1] - data->getDistances()[0];
}
