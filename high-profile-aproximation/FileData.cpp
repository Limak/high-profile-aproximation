#include "FileData.h"

FileData::FileData()
{
}

FileData::FileData(char * input, int testPointsAmount)
{
	swap(filename, input);
	pointsAmount = testPointsAmount;
	distances = new double[pointsAmount];
	heights = new double[pointsAmount];
}

void FileData::readData()
{
	ifstream file;
	file.open(filename);
	double tmpDistance = 0.0;
	double tmpHeight = 0.0;

	if (!file.is_open())
	{
		cout << "Nie znaleziono pliku" << endl;
		return;
	}
	else
		cout << "Plik: " << filename << endl;

	for (int i = 0; i < pointsAmount; i++)
	{
		file >> distances[i];
		double tmpDistance = distances[i];
		file >> heights[i];
	}
}

char * FileData::getFilename()
{
	return filename;
}

double * FileData::getDistances()
{
	return distances;
}

double * FileData::getHeights()
{
	return heights;
}

int FileData::getPointsAmount()
{
	return pointsAmount;
}

FileData::~FileData()
{
}
